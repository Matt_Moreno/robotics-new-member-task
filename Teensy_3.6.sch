EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Regulator_Switching:R-78B5.0-2.0 U1
U 1 1 6103CB37
P 3500 2550
F 0 "U1" H 3500 2792 50  0000 C CNN
F 1 "R-78B5.0-2.0" H 3500 2701 50  0000 C CNN
F 2 "Converter_DCDC:Converter_DCDC_RECOM_R-78B-2.0_THT" H 3550 2300 50  0001 L CIN
F 3 "https://www.recom-power.com/pdf/Innoline/R-78Bxx-2.0.pdf" H 3500 2550 50  0001 C CNN
	1    3500 2550
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 610463E6
P 2950 2700
F 0 "C1" H 3065 2746 50  0000 L CNN
F 1 "33u" H 3065 2655 50  0001 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 2988 2550 50  0001 C CNN
F 3 "~" H 2950 2700 50  0001 C CNN
	1    2950 2700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 6104AEC4
P 3650 5750
F 0 "#PWR0101" H 3650 5500 50  0001 C CNN
F 1 "GND" H 3655 5577 50  0000 C CNN
F 2 "" H 3650 5750 50  0001 C CNN
F 3 "" H 3650 5750 50  0001 C CNN
	1    3650 5750
	1    0    0    -1  
$EndComp
$Comp
L Device:Battery_Cell BT1
U 1 1 610541AA
P 1400 2800
F 0 "BT1" H 1518 2896 50  0000 L CNN
F 1 "Power Input" H 1518 2805 50  0000 L CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x02_P4.6mm_D0.9mm_OD2.1mm" V 1400 2860 50  0001 C CNN
F 3 "~" V 1400 2860 50  0001 C CNN
	1    1400 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 2550 2950 2550
Wire Wire Line
	1400 2550 1400 2600
Wire Wire Line
	3500 2850 3500 3250
Wire Wire Line
	3500 3250 2950 3250
Connection ~ 1400 3250
Wire Wire Line
	1400 5750 1400 3250
Wire Wire Line
	3950 2900 3950 3250
Wire Wire Line
	3950 3250 3500 3250
Connection ~ 3500 3250
Connection ~ 2950 2550
Wire Wire Line
	2950 2550 1400 2550
Wire Wire Line
	3800 2550 3950 2550
Wire Wire Line
	3950 2600 3950 2550
Connection ~ 3950 2550
Wire Wire Line
	1400 5750 3650 5750
Connection ~ 3650 5750
Wire Wire Line
	8350 4400 8350 5750
Wire Wire Line
	4100 3000 4100 3250
Wire Wire Line
	4100 3250 3950 3250
Connection ~ 3950 3250
Wire Wire Line
	1400 2900 1400 3250
Wire Wire Line
	2950 2850 2950 3250
Connection ~ 2950 3250
Wire Wire Line
	2950 3250 1400 3250
Wire Wire Line
	3650 5750 8350 5750
NoConn ~ 5900 5300
NoConn ~ 5900 5200
NoConn ~ 5900 5100
NoConn ~ 5900 5000
NoConn ~ 5900 4900
NoConn ~ 5900 4800
NoConn ~ 5900 4700
NoConn ~ 5900 4600
NoConn ~ 5900 4500
NoConn ~ 5900 4300
NoConn ~ 5900 4200
NoConn ~ 5900 4100
NoConn ~ 5900 4000
NoConn ~ 5900 3900
NoConn ~ 5900 3800
NoConn ~ 5900 3600
NoConn ~ 5900 3500
NoConn ~ 5900 3400
NoConn ~ 5900 3200
NoConn ~ 5900 3100
NoConn ~ 8250 5300
NoConn ~ 8250 5200
NoConn ~ 8250 5100
NoConn ~ 8250 5000
NoConn ~ 8250 4900
NoConn ~ 8250 4800
NoConn ~ 8250 4700
NoConn ~ 8250 4600
NoConn ~ 8250 4500
NoConn ~ 8250 4300
NoConn ~ 8250 4200
NoConn ~ 8250 4100
NoConn ~ 8250 4000
NoConn ~ 8250 3900
NoConn ~ 8250 3800
NoConn ~ 8250 3700
NoConn ~ 8250 3600
NoConn ~ 8250 3500
NoConn ~ 8250 3400
NoConn ~ 8250 3300
NoConn ~ 8250 3200
NoConn ~ 8250 3100
Wire Wire Line
	8250 2550 8250 3000
Wire Wire Line
	4100 3000 5100 3000
Wire Wire Line
	8350 4400 8250 4400
$Comp
L Teensy~3.6~(custom):Teensy3.6 U3
U 1 1 610D3263
P 5200 2950
F 0 "U3" H 7100 1600 50  0000 C CNN
F 1 "Teensy3.6" H 7100 1500 50  0000 C CNN
F 2 "Teensy 3.6 (custom footprint):Teensy 3.6" H 5200 2950 50  0001 C CNN
F 3 "" H 5200 2950 50  0001 C CNN
	1    5200 2950
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J1
U 1 1 610D8294
P 4850 2800
F 0 "J1" H 4958 3081 50  0000 C CNN
F 1 "Servo Motor Connector" H 4958 2990 50  0000 C CNN
F 2 "Connector_Wuerth:Wuerth_WR-WTB_64800311622_1x03_P1.50mm_Vertical" H 4850 2800 50  0001 C CNN
F 3 "~" H 4850 2800 50  0001 C CNN
	1    4850 2800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J2
U 1 1 610DDE5F
P 3650 3900
F 0 "J2" V 3600 4050 50  0000 L CNN
F 1 "DC Motor Driver Connector" V 3700 4050 50  0000 L CNN
F 2 "Connector_Wire:SolderWire-0.5sqmm_1x03_P4.6mm_D0.9mm_OD2.1mm" H 3650 3900 50  0001 C CNN
F 3 "~" H 3650 3900 50  0001 C CNN
	1    3650 3900
	0    1    1    0   
$EndComp
Wire Wire Line
	3950 2550 5550 2550
Wire Wire Line
	5550 2550 5550 2700
Wire Wire Line
	5550 2700 5050 2700
Connection ~ 5550 2550
Wire Wire Line
	5550 2550 8250 2550
Wire Wire Line
	5100 3000 5100 2900
Wire Wire Line
	5100 2900 5050 2900
Connection ~ 5100 3000
Wire Wire Line
	5100 3000 5900 3000
Wire Wire Line
	5400 3300 5400 2800
Wire Wire Line
	5400 2800 5050 2800
Wire Wire Line
	5400 3300 5900 3300
Wire Wire Line
	5900 4400 3750 4400
Wire Wire Line
	3750 4400 3750 4100
Wire Wire Line
	3650 4100 3650 5750
Wire Wire Line
	3550 4100 3550 4250
Wire Wire Line
	3550 4250 3350 4250
Wire Wire Line
	3350 4250 3350 3700
Wire Wire Line
	3350 3700 5900 3700
Text Label 1400 3550 1    50   ~ 0
GND
Text Label 3650 4500 1    50   ~ 0
GND
Text Label 3750 4250 0    50   ~ 0
Direction
Text Label 3350 4250 0    50   ~ 0
PWM
Text Label 5100 2700 0    50   ~ 0
Power
Text Label 5050 2900 0    50   ~ 0
GND
Text Label 5100 2800 0    50   ~ 0
PWM
$Comp
L Device:C C2
U 1 1 61046CB0
P 3950 2750
F 0 "C2" H 4065 2796 50  0000 L CNN
F 1 "33u" H 4065 2705 50  0001 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.18x1.45mm_HandSolder" H 3988 2600 50  0001 C CNN
F 3 "~" H 3950 2750 50  0001 C CNN
	1    3950 2750
	1    0    0    -1  
$EndComp
$EndSCHEMATC
