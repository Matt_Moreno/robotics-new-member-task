
#include <iostream>
using namespace std;

void vbattRead() //main function
{
//assumed that function analogRead() is already defined and it returns data type float
	float vmax = 3.3;//max voltage of MCU pin
	float r1 = 10000;//R1 resistor value
	float r2 = 2000;// R2 resistor value
	pinVal = analogRead(pinNum);// assumed analogRead() is defined, pinVal is assigned the value read from pin number (also assumed that pinval is defined and represents mcu pin)
	float vsense = pinVal*vmax/1023; //assume 
	float vbatt = (r1 + r2)*vsense/r2; //voltage division rule

	if (vbatt < 12 ){cout << "Battery voltage is too low (below 12V) !";}
		
		
	else if (vbatt > 16.8){cout << "Battery voltage is too high (above 16.8V) !";}
		
	
	else
		cout << "Current battery voltage: " << vbatt << "V";
	
}